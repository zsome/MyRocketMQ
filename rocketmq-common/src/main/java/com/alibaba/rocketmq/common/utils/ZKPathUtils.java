package com.alibaba.rocketmq.common.utils;

/**
 *
 */
public class ZKPathUtils {

	private static final String PATH_SEPARATOR = "/";

	private static final String META_SERVER_ASSIGNMENT_PATH_ROOT = "/metaserver-assignment";

	private static final String META_SERVER_ASSIGNMENT_PATH_PATTERN = META_SERVER_ASSIGNMENT_PATH_ROOT + "/%s";

	private static final String BrokersPath = "/brokers";

	private static final String ClusterPath = "/cluster";

	private static final String ConsumersPath = "/consumers";

	private static final String ConfigPath = "/config";

	private static final String ControllerPath = "/controller";

	private static final String ClusterIdPath = ClusterPath+"/id";

	private static final String BrokerIdsPath = BrokersPath+"/ids";

	private static final String BrokerTopicsPath = BrokersPath+"/topics";

	private static final String IsrChangeNotificationPath = "/isr_change_notification";

	public static String getBrokerRegistryBasePath() {
		return BrokersPath;
	}

	public static String getBrokerRegistryName(String name) {
		return "default-cluster";
	}

	public static String getBaseMetaVersionZkPath() {
		return "/base-meta-version";
	}

	public static String getMetaInfoZkPath() {
		return "/meta-info";
	}

	public static String getMetaServersZkPath() {
		return BrokersPath;
	}

	public static String lastSegment(String path) {
		int lastSlashIdx = path.lastIndexOf(PATH_SEPARATOR);

		if (lastSlashIdx >= 0) {
			return path.substring(lastSlashIdx + 1);
		} else {
			return path;
		}
	}

	public static String getMetaServerAssignmentRootZkPath() {
		return META_SERVER_ASSIGNMENT_PATH_ROOT;
	}

	public static String getMetaServerAssignmentZkPath(String topic) {
		return String.format(META_SERVER_ASSIGNMENT_PATH_PATTERN, topic);
	}

}
